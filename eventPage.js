var menuItem = {
  "id": "php_everywhere",
  "title": "PHP Function Lookup",
  "contexts": ["selection"]
};
chrome.contextMenus.create(menuItem);

function fixedEncodeURI(str){
  return encodeURI(str).replace(/%5/g, '[').replace(/%5D/g, ']');
}

function escapeHtml(text) {
    return text
        .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")
        .replace(/"/g, "&quot;")
        .replace(/'/g, "&#039;")
        .replace(/_/g, "-");
  }


chrome.contextMenus.onClicked.addListener(function(clickData){
  if (clickData.menuItemId == "php_everywhere" && clickData.selectionText)  {
    chrome.storage.sync.get(['color', 'language', 'display'], function(settings){
      var keyword = fixedEncodeURI(escapeHtml(clickData.selectionText))
      var new_language = settings.language ? settings.language : 'en';
      var display = settings.display ? settings.display : "tab";
      // alert(new_language);
      var url = "http://php.net/manual/" + new_language +  "/function." + keyword + ".php";
      var createData = {
        "url" : url,
        "type": "normal",
        "top": 5,
        "left": 5,
        "width": screen.availWidth/2,
        "height": screen.availHeight
      };

      if (display == "tab") {
        window.open(url,'_blank');
      } else{
        chrome.windows.create(createData, function(){});
      }
    });
  }
});
