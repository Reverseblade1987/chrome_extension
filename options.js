$(function(){

  chrome.storage.sync.get(['color', 'language', 'color2', 'color3', 'display'],function(settings){
  if (settings.language) {$('#language').val(settings.language);} else{
    $('#language').val("en");
  }

  if (settings.language === 'ja') {
    $('#page_title').html('設定画面');
    $('#man_lang').html('マニュアルの言語');
    $('#disp_opt').html('表示形式');
    $('#bg_color').html('背景色');
    $('#btn_color').html('ボタンの背景色');
    $('#font_color').html('フォントの色');
    $('#reset').html('リセット');
    $('#save').html('変更を保存');
    $('#close').html('閉じる');
    $('#language').children('[value="ja"]').html('日本語');
    $('#language').children('[value="en"]').html('英語');
    $('#display').children('[value="tab"]').html('新規タブ');
    $('#display').children('[value="window"]').html('新規ウィンドウ');
  }

  if (settings.color) { $('#fontColor').val(settings.color);} else{
     $('#fontColor').val("333");
  }

  if (settings.color2) {$('#fontColor2').val(settings.color2);} else{
    $('#fontColor2').val('694fff');
  }

  if (settings.color3) { $('#fontColor3').val(settings.color3);} else{
     $('#fontColor3').val('fff');
  }

  if (settings.display) { $('#display').val(settings.display);} else{
     $('#display').val('tab');
  }

  // $('#language').val(settings.language);
  // $('#fontColor').val(settings.color);
  // $('#fontColor2').val(settings.color2);
  // $('#fontColor3').val(settings.color3);
});

  $('#reset').click(function(){
    // alert("hello");
    chrome.storage.sync.set({'language': 'en'});
    chrome.storage.sync.set({'color': '333'});
    chrome.storage.sync.set({'color3': 'fff'});
    chrome.storage.sync.set({'display': 'tab'});
    chrome.storage.sync.set({'color2':'694FFF'}, function(){
      var notifOptions = {
              type: 'basic',
              iconUrl: "icon.png",
              title:'PHP Manual Everywhere',
              message: "Reset OK(設定をリセットしました)"
            };
      chrome.notifications.create('changeSaved', notifOptions);
      window.location.href = "";
    });
  });

  $('#close').click(function(){
    close();
  });

  $('#save').click(function(){
    // alert("hello");
    var language = $('#language').val();
    var display =  $('#display').val();
    console.log($('#display').val());
    var color =  $('#fontColor').val();
    var color2 =  $('#fontColor2').val();
    var color3 =  $('#fontColor3').val();
    chrome.storage.sync.set({'language': language});
    chrome.storage.sync.set({'display': display});
    chrome.storage.sync.set({'color2': color2});
    chrome.storage.sync.set({'color3': color3});
    chrome.storage.sync.set({'color':color}, function(){
       var notifOptions = {
              type: 'basic',
              iconUrl: "icon.png",
              title:'PHP Manual Everywhere',
              message: "Changes Saved(変更を保存しました！)"
            };
      chrome.notifications.create('changeSaved', notifOptions);


      location.reload();
      // window.location.href = "";
    });
  });
});
